import lists.ArrayListReverse;
import lists.HashMapWordCount;
import lists.HashSetIterator;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Main {
    private static final Logger log = Logger.getLogger(Main.class);

    public static void main(String[] args) {
        ArrayListReverse listReverse = new ArrayListReverse();
        HashMapWordCount hashMapCount = new HashMapWordCount();
        HashSetIterator hashSetIterator = new HashSetIterator();
        ArrayList<Integer> arrayList = new ArrayList<>();
        Map<String, Integer> hashMap = new HashMap<>();
        HashSet<Integer> hashSet = new HashSet<>();


        listReverse.addElement(arrayList);
        listReverse.printElements(arrayList);
        listReverse.reverseArrayList(arrayList);

        hashMapCount.addElements(hashMap);
        hashMapCount.wordCount(hashMap);

        hashSetIterator.hashSetAdd(hashSet);
        hashSetIterator.hashSetIterator(hashSet);
    }
}
