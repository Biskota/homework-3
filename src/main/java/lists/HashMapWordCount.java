package lists;

import org.apache.log4j.Logger;

import java.util.*;

public class HashMapWordCount {
    private static final Logger log = Logger.getLogger(HashMapWordCount.class);

    public void addElements(Map<String, Integer> hashMap) {
        String sentence = "potato, banana, tomato, potato, avocado, potato";
        log.info("String: " + sentence);
        String[] strings = sentence.split(", ");
        for (String string : strings) {
            if (hashMap.containsKey(string)) {
                int count = hashMap.get(string);
                hashMap.put(string, count + 1);
            } else {
                hashMap.put(string, 1);
            }
        }
    }

    /**
     * Count amount of concrete word in String using HashMap.
     *
     * @param hashMap
     */
    public void wordCount(Map<String, Integer> hashMap) {
        String word = "potato";
        log.info("Word: " + word + ", count: " + hashMap.get(word));
    }


}
