package lists;

import org.apache.log4j.Logger;

import java.util.*;


public class HashSetIterator {
    private static final Logger log = Logger.getLogger(HashSetIterator.class);

    /**
     * Get a collection view(Hash Set) of the values contained in this map
     */
    public void hashSetAdd(HashSet<Integer> hashSet) {
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            hashSet.add(new Integer(random.nextInt(20)));
        }
        log.info(hashSet);
    }

    /**
     * Iterate through all elements in a hash list
     */
    public void hashSetIterator(HashSet<Integer> hashSet) {
        Iterator<Integer> p = hashSet.iterator();
        while (p.hasNext()) {
            //System.out.print(p.next() + " ");
            log.info(p.next());
        }

    }
}
