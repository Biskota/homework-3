package lists;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Random;

public class ArrayListReverse {
    private static final Logger log = Logger.getLogger(ArrayListReverse.class);

    public void addElement(ArrayList<Integer> arrayList) {
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            arrayList.add(new Integer(random.nextInt(10)));
        }
    }

    /**
     * Print all the elements of a ArrayList using the position of the elements.
     *
     * @param arrayList
     */
    public void printElements(ArrayList<Integer> arrayList) {
        for (int i = 0; i < arrayList.size(); i++) {
           //System.out.print(arrayList.get(i) + " ");
            log.info(arrayList.get(i));
        }
        log.info("Elements array: " + arrayList);
    }

    /**
     * Reverse Array List
     *
     * @param arrayList
     */
    public void reverseArrayList(ArrayList<Integer> arrayList) {
        ArrayList<Integer> revArrayList = new ArrayList<>();
        for (int i = arrayList.size() - 1; i >= 0; i--) {
            revArrayList.add(arrayList.get(i));
        }
        log.info("Elements after reversing: " + revArrayList);

    }

}
